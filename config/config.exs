# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :wisdom_tooth,
  ecto_repos: [WisdomTooth.Repo]

# Configures the endpoint
config :wisdom_tooth, WisdomToothWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "gXd5CpnvakNS08Zv8BciGaakYuF1pAVROwZ6swqJMdxxcwOXT6HnNA0w1drVirHW",
  render_errors: [view: WisdomToothWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: WisdomTooth.PubSub,
           adapter: Phoenix.PubSub.PG2]


# config :wisdom_tooth, WisdomTooth.Mailer,
#   adapter: Bamboo.LocalAdapter

#config :wisdom_tooth, WisdomTooth.Mailer,
#  adapter: Bamboo.MailgunAdapter,
#  api_key: "key-6azlrkdasvwhab-e6c-pwiq4ahdh9456",
#  domain: "juicios.antifaz.cl"


# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :wisdom_tooth, WisdomToothWeb.Gettext, default_locale: "es"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"