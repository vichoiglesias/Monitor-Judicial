defmodule WisdomTooth.Repo.Migrations.ReplaceLitigantesUniqueIndex do
  use Ecto.Migration

  def change do
    drop index(:litigantes, [:rut, :juicio_id])

    create index(:litigantes, [:rol, :rut, :juicio_id], unique: true)
  end
end
