defmodule WisdomTooth.Repo.Migrations.AddMetadataToJuicio do
  use Ecto.Migration

  def change do
    alter table("juicios") do
      add :estado_administrativo, :string
      add :estado_procedimiento, :string
      add :etapa, :string
      add :url_demanda, :string
      add :procedimiento, :string
      add :ubicacion, :string
    end
  end
end
