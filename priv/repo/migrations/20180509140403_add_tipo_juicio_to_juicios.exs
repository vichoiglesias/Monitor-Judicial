defmodule WisdomTooth.Repo.Migrations.AddTipoJuicioToJuicios do
  use Ecto.Migration

  def change do
    alter table("juicios") do
      add :tipo, :string
    end
  end
end
