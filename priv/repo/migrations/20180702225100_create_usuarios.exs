defmodule WisdomTooth.Repo.Migrations.CreateUsuarios do
  use Ecto.Migration

  def change do
    create table(:usuarios) do
      add :nombre, :string
      add :email, :string
      add :password_hash, :string

      timestamps()
    end

  end
end
