defmodule WisdomTooth.Repo.Migrations.AddInterlexDataToJuicios do
  use Ecto.Migration

  def change do
    alter table("juicios") do
      add :contraparte, :string
      add :rut_contraparte, :string
      add :tipo_procedimiento, :string
      add :calidad, :string
      add :cuantia, :integer
      add :abogado_contraparte, :string
    end
  end
end
