defmodule WisdomTooth.Repo.Migrations.CreateEscritosPorResolver do
  use Ecto.Migration

  def change do
    create table(:escritos_por_resolver) do
      add :documento, :string
      add :anexo, :string
      add :fecha_ingreso, :date
      add :tipo_escrito, :string
      add :solicitante, :string
      add :notificado, :date
      add :juicio_id, references(:juicios, on_delete: :nothing)

      timestamps()
    end

    create index(:escritos_por_resolver, [:juicio_id])
    create index(:escritos_por_resolver, [:documento, :anexo, :fecha_ingreso, :tipo_escrito, :solicitante, :juicio_id], unique: true)
  end
end
