defmodule WisdomTooth.Repo.Migrations.CreateExhortos do
  use Ecto.Migration

  def change do
    create table(:exhortos) do
      add :fecha_orden, :date
      add :fecha_ingreso, :date
      add :tribunal, :string
      add :estado, :string
      add :juicio_origen_id, references(:juicios, on_delete: :nothing)
      add :juicio_destino_id, references(:juicios, on_delete: :nothing)

      timestamps()
    end

    create index(:exhortos, [:juicio_origen_id])
    create index(:exhortos, [:juicio_destino_id])
    create index(:exhortos, [:juicio_origen_id, :juicio_destino_id], unique: true)
  end
end
