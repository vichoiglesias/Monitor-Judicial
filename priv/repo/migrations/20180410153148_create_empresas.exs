defmodule WisdomTooth.Repo.Migrations.CreateEmpresas do
  use Ecto.Migration

  def change do
    create table(:empresas) do
      add :nombre, :string
      add :rut, :string

      timestamps()
    end

    create index(:empresas, [:rut], unique: true)
  end
end
