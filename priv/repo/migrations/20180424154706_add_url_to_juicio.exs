defmodule WisdomTooth.Repo.Migrations.AddURLToJuicio do
  use Ecto.Migration

  def change do
    alter table("juicios") do
      add :url, :string
    end
  end
end
