defmodule WisdomTooth.Repo.Migrations.CreateJuicios do
  use Ecto.Migration

  def change do
    create table(:juicios) do
      add :rol, :string
      add :fecha, :date
      add :caratulado, :string
      add :tribunal, :string
      add :rut, :string
      add :notificado, :date
      add :last_seen, :date

      timestamps()
    end

    create index(:juicios, [:rol, :tribunal, :rut], unique: true)
  end
end
