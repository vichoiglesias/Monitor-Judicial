defmodule WisdomTooth.Repo.Migrations.CreateLitigantes do
  use Ecto.Migration

  def change do
    create table(:litigantes) do
      add :rol, :string
      add :rut, :string
      add :tipo_persona, :string
      add :nombre, :string
      add :juicio_id, references(:juicios, on_delete: :nothing)

      timestamps()
    end

    create index(:litigantes, [:juicio_id])
    create index(:litigantes, [:rut, :juicio_id], unique: true)
  end
end
