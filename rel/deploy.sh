echo Destilando la aplicación
cd assets && node node_modules/webpack/bin/webpack.js && cd .. && MIX_ENV=prod mix do phx.digest, release --env=prod

echo Enviándola a Holanda
scp _build/prod/rel/wisdom_tooth/releases/0.0.1/wisdom_tooth.tar.gz root@juicios.interlex.cl:/home/wt.tar.gz

echo Avisándole a los Holandeses que hacer
ssh -t -t root@juicios.interlex.cl '
cd /home/wt
./bin/wisdom_tooth stop
cd ..
rm -rf wt
mkdir wt
tar -xf wt.tar.gz -C wt/
cd wt
PORT=4000 ./bin/wisdom_tooth start
'
