defmodule WisdomTooth.CuentasTest do
  use WisdomTooth.DataCase

  alias WisdomTooth.Cuentas

  describe "usuarios" do
    alias WisdomTooth.Cuentas.Usuario

    @valid_attrs %{email: "some email", nombre: "some nombre", password_hash: "some password_hash"}
    @update_attrs %{email: "some updated email", nombre: "some updated nombre", password_hash: "some updated password_hash"}
    @invalid_attrs %{email: nil, nombre: nil, password_hash: nil}

    def usuario_fixture(attrs \\ %{}) do
      {:ok, usuario} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Cuentas.create_usuario()

      usuario
    end

    test "list_usuarios/0 returns all usuarios" do
      usuario = usuario_fixture()
      assert Cuentas.list_usuarios() == [usuario]
    end

    test "get_usuario!/1 returns the usuario with given id" do
      usuario = usuario_fixture()
      assert Cuentas.get_usuario!(usuario.id) == usuario
    end

    test "create_usuario/1 with valid data creates a usuario" do
      assert {:ok, %Usuario{} = usuario} = Cuentas.create_usuario(@valid_attrs)
      assert usuario.email == "some email"
      assert usuario.nombre == "some nombre"
      assert usuario.password_hash == "some password_hash"
    end

    test "create_usuario/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Cuentas.create_usuario(@invalid_attrs)
    end

    test "update_usuario/2 with valid data updates the usuario" do
      usuario = usuario_fixture()
      assert {:ok, usuario} = Cuentas.update_usuario(usuario, @update_attrs)
      assert %Usuario{} = usuario
      assert usuario.email == "some updated email"
      assert usuario.nombre == "some updated nombre"
      assert usuario.password_hash == "some updated password_hash"
    end

    test "update_usuario/2 with invalid data returns error changeset" do
      usuario = usuario_fixture()
      assert {:error, %Ecto.Changeset{}} = Cuentas.update_usuario(usuario, @invalid_attrs)
      assert usuario == Cuentas.get_usuario!(usuario.id)
    end

    test "delete_usuario/1 deletes the usuario" do
      usuario = usuario_fixture()
      assert {:ok, %Usuario{}} = Cuentas.delete_usuario(usuario)
      assert_raise Ecto.NoResultsError, fn -> Cuentas.get_usuario!(usuario.id) end
    end

    test "change_usuario/1 returns a usuario changeset" do
      usuario = usuario_fixture()
      assert %Ecto.Changeset{} = Cuentas.change_usuario(usuario)
    end
  end
end
