defmodule WisdomTooth.GestionJuiciosTest do
  use WisdomTooth.DataCase

  alias WisdomTooth.GestionJuicios

  describe "juicios" do
    alias WisdomTooth.GestionJuicios.Juicio

    @valid_attrs %{caratulado: "some caratulado", fecha: ~D[2010-04-17], rol: "some rol", rut: "some rut", tribunal: "some tribunal"}
    @update_attrs %{caratulado: "some updated caratulado", fecha: ~D[2011-05-18], rol: "some updated rol", rut: "some updated rut", tribunal: "some updated tribunal"}
    @invalid_attrs %{caratulado: nil, fecha: nil, rol: nil, rut: nil, tribunal: nil}

    def juicio_fixture(attrs \\ %{}) do
      {:ok, juicio} =
        attrs
        |> Enum.into(@valid_attrs)
        |> GestionJuicios.create_juicio()

      juicio
    end

    test "list_juicios/0 returns all juicios" do
      juicio = juicio_fixture()
      assert GestionJuicios.list_juicios() == [juicio]
    end

    test "get_juicio!/1 returns the juicio with given id" do
      juicio = juicio_fixture()
      assert GestionJuicios.get_juicio!(juicio.id) == juicio
    end

    test "create_juicio/1 with valid data creates a juicio" do
      assert {:ok, %Juicio{} = juicio} = GestionJuicios.create_juicio(@valid_attrs)
      assert juicio.caratulado == "some caratulado"
      assert juicio.fecha == ~D[2010-04-17]
      assert juicio.rol == "some rol"
      assert juicio.rut == "some rut"
      assert juicio.tribunal == "some tribunal"
    end

    test "create_juicio/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = GestionJuicios.create_juicio(@invalid_attrs)
    end

    test "update_juicio/2 with valid data updates the juicio" do
      juicio = juicio_fixture()
      assert {:ok, juicio} = GestionJuicios.update_juicio(juicio, @update_attrs)
      assert %Juicio{} = juicio
      assert juicio.caratulado == "some updated caratulado"
      assert juicio.fecha == ~D[2011-05-18]
      assert juicio.rol == "some updated rol"
      assert juicio.rut == "some updated rut"
      assert juicio.tribunal == "some updated tribunal"
    end

    test "update_juicio/2 with invalid data returns error changeset" do
      juicio = juicio_fixture()
      assert {:error, %Ecto.Changeset{}} = GestionJuicios.update_juicio(juicio, @invalid_attrs)
      assert juicio == GestionJuicios.get_juicio!(juicio.id)
    end

    test "delete_juicio/1 deletes the juicio" do
      juicio = juicio_fixture()
      assert {:ok, %Juicio{}} = GestionJuicios.delete_juicio(juicio)
      assert_raise Ecto.NoResultsError, fn -> GestionJuicios.get_juicio!(juicio.id) end
    end

    test "change_juicio/1 returns a juicio changeset" do
      juicio = juicio_fixture()
      assert %Ecto.Changeset{} = GestionJuicios.change_juicio(juicio)
    end
  end

  describe "empresas" do
    alias WisdomTooth.GestionJuicios.Empresa

    @valid_attrs %{nombre: "some nombre", rut: "some rut"}
    @update_attrs %{nombre: "some updated nombre", rut: "some updated rut"}
    @invalid_attrs %{nombre: nil, rut: nil}

    def empresa_fixture(attrs \\ %{}) do
      {:ok, empresa} =
        attrs
        |> Enum.into(@valid_attrs)
        |> GestionJuicios.create_empresa()

      empresa
    end

    test "list_empresas/0 returns all empresas" do
      empresa = empresa_fixture()
      assert GestionJuicios.list_empresas() == [empresa]
    end

    test "get_empresa!/1 returns the empresa with given id" do
      empresa = empresa_fixture()
      assert GestionJuicios.get_empresa!(empresa.id) == empresa
    end

    test "create_empresa/1 with valid data creates a empresa" do
      assert {:ok, %Empresa{} = empresa} = GestionJuicios.create_empresa(@valid_attrs)
      assert empresa.nombre == "some nombre"
      assert empresa.rut == "some rut"
    end

    test "create_empresa/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = GestionJuicios.create_empresa(@invalid_attrs)
    end

    test "update_empresa/2 with valid data updates the empresa" do
      empresa = empresa_fixture()
      assert {:ok, empresa} = GestionJuicios.update_empresa(empresa, @update_attrs)
      assert %Empresa{} = empresa
      assert empresa.nombre == "some updated nombre"
      assert empresa.rut == "some updated rut"
    end

    test "update_empresa/2 with invalid data returns error changeset" do
      empresa = empresa_fixture()
      assert {:error, %Ecto.Changeset{}} = GestionJuicios.update_empresa(empresa, @invalid_attrs)
      assert empresa == GestionJuicios.get_empresa!(empresa.id)
    end

    test "delete_empresa/1 deletes the empresa" do
      empresa = empresa_fixture()
      assert {:ok, %Empresa{}} = GestionJuicios.delete_empresa(empresa)
      assert_raise Ecto.NoResultsError, fn -> GestionJuicios.get_empresa!(empresa.id) end
    end

    test "change_empresa/1 returns a empresa changeset" do
      empresa = empresa_fixture()
      assert %Ecto.Changeset{} = GestionJuicios.change_empresa(empresa)
    end
  end

  describe "litigantes" do
    alias WisdomTooth.GestionJuicios.Litigante

    @valid_attrs %{nombre: "some nombre", rol: "some rol", rut: "some rut", tipo_persona: "some tipo_persona"}
    @update_attrs %{nombre: "some updated nombre", rol: "some updated rol", rut: "some updated rut", tipo_persona: "some updated tipo_persona"}
    @invalid_attrs %{nombre: nil, rol: nil, rut: nil, tipo_persona: nil}

    def litigante_fixture(attrs \\ %{}) do
      {:ok, litigante} =
        attrs
        |> Enum.into(@valid_attrs)
        |> GestionJuicios.create_litigante()

      litigante
    end

    test "list_litigantes/0 returns all litigantes" do
      litigante = litigante_fixture()
      assert GestionJuicios.list_litigantes() == [litigante]
    end

    test "get_litigante!/1 returns the litigante with given id" do
      litigante = litigante_fixture()
      assert GestionJuicios.get_litigante!(litigante.id) == litigante
    end

    test "create_litigante/1 with valid data creates a litigante" do
      assert {:ok, %Litigante{} = litigante} = GestionJuicios.create_litigante(@valid_attrs)
      assert litigante.nombre == "some nombre"
      assert litigante.rol == "some rol"
      assert litigante.rut == "some rut"
      assert litigante.tipo_persona == "some tipo_persona"
    end

    test "create_litigante/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = GestionJuicios.create_litigante(@invalid_attrs)
    end

    test "update_litigante/2 with valid data updates the litigante" do
      litigante = litigante_fixture()
      assert {:ok, litigante} = GestionJuicios.update_litigante(litigante, @update_attrs)
      assert %Litigante{} = litigante
      assert litigante.nombre == "some updated nombre"
      assert litigante.rol == "some updated rol"
      assert litigante.rut == "some updated rut"
      assert litigante.tipo_persona == "some updated tipo_persona"
    end

    test "update_litigante/2 with invalid data returns error changeset" do
      litigante = litigante_fixture()
      assert {:error, %Ecto.Changeset{}} = GestionJuicios.update_litigante(litigante, @invalid_attrs)
      assert litigante == GestionJuicios.get_litigante!(litigante.id)
    end

    test "delete_litigante/1 deletes the litigante" do
      litigante = litigante_fixture()
      assert {:ok, %Litigante{}} = GestionJuicios.delete_litigante(litigante)
      assert_raise Ecto.NoResultsError, fn -> GestionJuicios.get_litigante!(litigante.id) end
    end

    test "change_litigante/1 returns a litigante changeset" do
      litigante = litigante_fixture()
      assert %Ecto.Changeset{} = GestionJuicios.change_litigante(litigante)
    end
  end
end
