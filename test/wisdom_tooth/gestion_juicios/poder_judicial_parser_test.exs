defmodule WisdomTooth.GestionJuiciosTest.PoderJudicialParserTest do
  use WisdomTooth.DataCase
  alias WisdomTooth.GestionJuicios.PoderJudicialParser

  setup_all do
    rut = "96.951.870-8"
    juicios = PoderJudicialParser.find_for_rut(rut)
    juicio = Enum.find(juicios, fn(juicio) -> juicio.rol == "C-4372-2017" end)

    [rut: rut, juicios: juicios, juicio: juicio]
  end


  test "Load Cookie Civil" do
    assert PoderJudicialParser.initial_payload_civil =~ "JSESSIONID"
  end

  test "Load Cookie Laboral" do
    assert PoderJudicialParser.initial_payload_laboral =~ "JSESSIONID"
  end

  test "Obtener Juicios", context do
    assert is_list(context[:juicios]), "Error al obtener juicios"
  end

  test "Hay más de los juicios previamente detectados", context do
    assert length(context[:juicios]) >= 55
  end

  test "Todos los RUTs son iguales", context do
    assert Enum.all?(context[:juicios], fn(juicio) -> juicio.rut == context[:rut] end), "No todos los RUT son iguales!"
  end

  test "Todas las fechas son de la structura Date", context do
    assert Enum.all?(context[:juicios], fn(juicio) -> juicio.fecha.__struct__ == Date end), "No todos las fechas son del tipo fecha!"
  end

  test "Caratulado ok", context do
    assert context[:juicio].caratulado == "ARAYA/BIONET S.A."
  end

  test "Tipo de juicio OK", context do
    assert context[:juicio].tipo == "Civil"
  end

  test "Tribunal OK", context do
    assert context[:juicio].tribunal == "4º Juzgado de Letras Civil de Antofagasta"
  end

  test "Juicios solo laborales", context do
    juicios_laborales = PoderJudicialParser.find_for_rut_laboral(context[:rut])

    assert Enum.all?(juicios_laborales, fn(juicio) -> juicio.tipo == "Laboral" end), "No todos juicios son laborales!"
  end
end
