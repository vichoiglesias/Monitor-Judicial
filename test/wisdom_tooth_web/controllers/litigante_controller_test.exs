defmodule WisdomToothWeb.LitiganteControllerTest do
  use WisdomToothWeb.ConnCase

  alias WisdomTooth.GestionJuicios

  @create_attrs %{nombre: "some nombre", rol: "some rol", rut: "some rut", tipo_persona: "some tipo_persona"}
  @update_attrs %{nombre: "some updated nombre", rol: "some updated rol", rut: "some updated rut", tipo_persona: "some updated tipo_persona"}
  @invalid_attrs %{nombre: nil, rol: nil, rut: nil, tipo_persona: nil}

  def fixture(:litigante) do
    {:ok, litigante} = GestionJuicios.create_litigante(@create_attrs)
    litigante
  end

  describe "index" do
    test "lists all litigantes", %{conn: conn} do
      conn = get conn, litigante_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Litigantes"
    end
  end

  describe "new litigante" do
    test "renders form", %{conn: conn} do
      conn = get conn, litigante_path(conn, :new)
      assert html_response(conn, 200) =~ "New Litigante"
    end
  end

  describe "create litigante" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, litigante_path(conn, :create), litigante: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == litigante_path(conn, :show, id)

      conn = get conn, litigante_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Litigante"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, litigante_path(conn, :create), litigante: @invalid_attrs
      assert html_response(conn, 200) =~ "New Litigante"
    end
  end

  describe "edit litigante" do
    setup [:create_litigante]

    test "renders form for editing chosen litigante", %{conn: conn, litigante: litigante} do
      conn = get conn, litigante_path(conn, :edit, litigante)
      assert html_response(conn, 200) =~ "Edit Litigante"
    end
  end

  describe "update litigante" do
    setup [:create_litigante]

    test "redirects when data is valid", %{conn: conn, litigante: litigante} do
      conn = put conn, litigante_path(conn, :update, litigante), litigante: @update_attrs
      assert redirected_to(conn) == litigante_path(conn, :show, litigante)

      conn = get conn, litigante_path(conn, :show, litigante)
      assert html_response(conn, 200) =~ "some updated nombre"
    end

    test "renders errors when data is invalid", %{conn: conn, litigante: litigante} do
      conn = put conn, litigante_path(conn, :update, litigante), litigante: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Litigante"
    end
  end

  describe "delete litigante" do
    setup [:create_litigante]

    test "deletes chosen litigante", %{conn: conn, litigante: litigante} do
      conn = delete conn, litigante_path(conn, :delete, litigante)
      assert redirected_to(conn) == litigante_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, litigante_path(conn, :show, litigante)
      end
    end
  end

  defp create_litigante(_) do
    litigante = fixture(:litigante)
    {:ok, litigante: litigante}
  end
end
