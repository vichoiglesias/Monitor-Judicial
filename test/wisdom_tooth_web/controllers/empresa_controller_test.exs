defmodule WisdomToothWeb.EmpresaControllerTest do
  use WisdomToothWeb.ConnCase

  alias WisdomTooth.GestionJuicios

  @create_attrs %{nombre: "some nombre", rut: "some rut"}
  @update_attrs %{nombre: "some updated nombre", rut: "some updated rut"}
  @invalid_attrs %{nombre: nil, rut: nil}

  def fixture(:empresa) do
    {:ok, empresa} = GestionJuicios.create_empresa(@create_attrs)
    empresa
  end

  describe "index" do
    test "lists all empresas", %{conn: conn} do
      conn = get conn, empresa_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Empresas"
    end
  end

  describe "new empresa" do
    test "renders form", %{conn: conn} do
      conn = get conn, empresa_path(conn, :new)
      assert html_response(conn, 200) =~ "New Empresa"
    end
  end

  describe "create empresa" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, empresa_path(conn, :create), empresa: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == empresa_path(conn, :show, id)

      conn = get conn, empresa_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Empresa"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, empresa_path(conn, :create), empresa: @invalid_attrs
      assert html_response(conn, 200) =~ "New Empresa"
    end
  end

  describe "edit empresa" do
    setup [:create_empresa]

    test "renders form for editing chosen empresa", %{conn: conn, empresa: empresa} do
      conn = get conn, empresa_path(conn, :edit, empresa)
      assert html_response(conn, 200) =~ "Edit Empresa"
    end
  end

  describe "update empresa" do
    setup [:create_empresa]

    test "redirects when data is valid", %{conn: conn, empresa: empresa} do
      conn = put conn, empresa_path(conn, :update, empresa), empresa: @update_attrs
      assert redirected_to(conn) == empresa_path(conn, :show, empresa)

      conn = get conn, empresa_path(conn, :show, empresa)
      assert html_response(conn, 200) =~ "some updated nombre"
    end

    test "renders errors when data is invalid", %{conn: conn, empresa: empresa} do
      conn = put conn, empresa_path(conn, :update, empresa), empresa: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Empresa"
    end
  end

  describe "delete empresa" do
    setup [:create_empresa]

    test "deletes chosen empresa", %{conn: conn, empresa: empresa} do
      conn = delete conn, empresa_path(conn, :delete, empresa)
      assert redirected_to(conn) == empresa_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, empresa_path(conn, :show, empresa)
      end
    end
  end

  defp create_empresa(_) do
    empresa = fixture(:empresa)
    {:ok, empresa: empresa}
  end
end
