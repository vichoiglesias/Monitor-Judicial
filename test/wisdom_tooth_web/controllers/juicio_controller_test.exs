defmodule WisdomToothWeb.JuicioControllerTest do
  use WisdomToothWeb.ConnCase

  alias WisdomTooth.GestionJuicios

  @create_attrs %{caratulado: "some caratulado", fecha: ~D[2010-04-17], rol: "some rol", rut: "some rut", tribunal: "some tribunal"}
  @update_attrs %{caratulado: "some updated caratulado", fecha: ~D[2011-05-18], rol: "some updated rol", rut: "some updated rut", tribunal: "some updated tribunal"}
  @invalid_attrs %{caratulado: nil, fecha: nil, rol: nil, rut: nil, tribunal: nil}

  def fixture(:juicio) do
    {:ok, juicio} = GestionJuicios.create_juicio(@create_attrs)
    juicio
  end

  describe "index" do
    test "lists all juicios", %{conn: conn} do
      conn = get conn, juicio_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Juicios"
    end
  end

  describe "new juicio" do
    test "renders form", %{conn: conn} do
      conn = get conn, juicio_path(conn, :new)
      assert html_response(conn, 200) =~ "New Juicio"
    end
  end

  describe "create juicio" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, juicio_path(conn, :create), juicio: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == juicio_path(conn, :show, id)

      conn = get conn, juicio_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Juicio"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, juicio_path(conn, :create), juicio: @invalid_attrs
      assert html_response(conn, 200) =~ "New Juicio"
    end
  end

  describe "edit juicio" do
    setup [:create_juicio]

    test "renders form for editing chosen juicio", %{conn: conn, juicio: juicio} do
      conn = get conn, juicio_path(conn, :edit, juicio)
      assert html_response(conn, 200) =~ "Edit Juicio"
    end
  end

  describe "update juicio" do
    setup [:create_juicio]

    test "redirects when data is valid", %{conn: conn, juicio: juicio} do
      conn = put conn, juicio_path(conn, :update, juicio), juicio: @update_attrs
      assert redirected_to(conn) == juicio_path(conn, :show, juicio)

      conn = get conn, juicio_path(conn, :show, juicio)
      assert html_response(conn, 200) =~ "some updated caratulado"
    end

    test "renders errors when data is invalid", %{conn: conn, juicio: juicio} do
      conn = put conn, juicio_path(conn, :update, juicio), juicio: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Juicio"
    end
  end

  describe "delete juicio" do
    setup [:create_juicio]

    test "deletes chosen juicio", %{conn: conn, juicio: juicio} do
      conn = delete conn, juicio_path(conn, :delete, juicio)
      assert redirected_to(conn) == juicio_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, juicio_path(conn, :show, juicio)
      end
    end
  end

  defp create_juicio(_) do
    juicio = fixture(:juicio)
    {:ok, juicio: juicio}
  end
end
