defmodule Release.Tasks do
  def migrate do
    {:ok, _} = Application.ensure_all_started(:wisdom_tooth)

    path = Application.app_dir(:wisdom_tooth, "priv/repo/migrations")

    Ecto.Migrator.run(WisdomTooth.Repo, path, :up, all: true)
  end
end
