defmodule WisdomToothWeb.Router do
  use WisdomToothWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug WisdomTooth.Auth
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  if Mix.env == :dev do
    forward "/sent_emails", Bamboo.EmailPreviewPlug
  end

  scope "/", WisdomToothWeb do
    pipe_through [:browser]
    
    scope "/" do
      pipe_through [:authenticate_user]
      get "/juicios/buscar", JuicioController, :buscar
      get "/juicios/detalle/:id/cuaderno", JuicioController, :cuaderno
      get "/juicios/detalle/:id", JuicioController, :detalle
      resources "/juicios", JuicioController

      resources "/litigantes", LitiganteController

      get "/empresas/buscar", EmpresaController, :buscar
      resources "/empresas", EmpresaController
    
      resources "/usuarios", UsuarioController
      
      get "/", PageController, :index
    end
    
    resources "/sesiones", SesionController, only: [:new, :create, :delete]
    
  end

  # Other scopes may use custom stacks.
  # scope "/api", WisdomToothWeb do
  #   pipe_through :api
  # end
end
