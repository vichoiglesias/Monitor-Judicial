defmodule WisdomToothWeb.JuicioController do
  use WisdomToothWeb, :controller

  alias WisdomTooth.GestionJuicios
  alias WisdomTooth.GestionJuicios.Juicio

  def index(conn, params) do
    {juicios, pagination} = GestionJuicios.list_juicios_paginated(params)
    render(conn, "index.html", juicios: juicios, pagination: pagination, busqueda: nil)
  end

  def new(conn, _params) do
    changeset = GestionJuicios.change_juicio(%Juicio{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"juicio" => juicio_params}) do
    case GestionJuicios.create_juicio(juicio_params) do
      {:ok, juicio} ->
        conn
        |> put_flash(:info, "Juicio created successfully.")
        |> redirect(to: juicio_path(conn, :show, juicio))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    juicio = GestionJuicios.get_juicio!(id)
    render(conn, "show.html", juicio: juicio)
  end

  def edit(conn, %{"id" => id}) do
    juicio = GestionJuicios.get_juicio!(id)
    changeset = GestionJuicios.change_juicio(juicio)
    render(conn, "edit.html", juicio: juicio, changeset: changeset)
  end

  def update(conn, %{"id" => id, "juicio" => juicio_params}) do
    juicio = GestionJuicios.get_juicio!(id)

    case GestionJuicios.update_juicio(juicio, juicio_params) do
      {:ok, juicio} ->
        conn
        |> put_flash(:info, "Juicio updated successfully.")
        |> redirect(to: juicio_path(conn, :show, juicio))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", juicio: juicio, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    juicio = GestionJuicios.get_juicio!(id)
    {:ok, _juicio} = GestionJuicios.delete_juicio(juicio)

    conn
    |> put_flash(:info, "Juicio deleted successfully.")
    |> redirect(to: juicio_path(conn, :index))
  end

  def detalle(conn, %{"id" => id}) do
    juicio = GestionJuicios.get_juicio!(id)

    html conn, GestionJuicios.PoderJudicialParser.cargar_pagina_detalle_juicio(juicio.url, id)
  end

  def cuaderno(conn, %{"id"=> id} = params) do
    juicio = GestionJuicios.get_juicio!(id)

    html conn, GestionJuicios.PoderJudicialParser.cargar_cuaderno(params, id, juicio.url)
  end

  def buscar(conn, %{"s" => busqueda, "page" => page}) do
    {juicios, pagination} = GestionJuicios.list_juicios_paginated_by_rol_o_caratulado(busqueda, %{"page" => page})

    render(conn, "index.html", juicios: juicios, pagination: pagination, busqueda: busqueda)
  end
  def buscar(conn, %{"s" => busqueda}), do: buscar(conn, %{"s" => busqueda, "page" => 1})
end
