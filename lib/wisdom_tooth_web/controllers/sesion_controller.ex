defmodule WisdomToothWeb.SesionController do
  use WisdomToothWeb, :controller
  alias WisdomTooth.Cuentas
  alias WisdomTooth.Auth
  
  def new(conn, _) do
    if conn.assigns.usuario do
      redirect conn, to: page_path(conn, :index)
    else
      render conn, "new.html"
    end
  end
  
  def create(conn, %{"sesion" => %{"email" => email, "password" => password}}) do
    case Auth.login_by_email_and_password(conn, email, password) do
      {:ok, conn} ->
        usuario = Cuentas.get_usuario_by_email(email)
        
        path = get_session(conn, :redirect_to) || page_path(conn, :index)
        
        conn
        |> delete_session(:redirect_to)
        |> put_flash(:info, "Bienvenido #{usuario.nombre}!")
        |> redirect(to: path)
      {:error, motivo} ->
        conn
        |> put_flash(:error, "Combinación de email y contraseña inválida")
        |> render("new.html")
    end
  end
  
  def delete(conn, _params) do
    conn
    |> Auth.logout()
    |> put_flash(:info, "Sesión finalizada")
    |> redirect(to: sesion_path(conn, :new))
  end
end