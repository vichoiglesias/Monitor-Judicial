defmodule WisdomToothWeb.EmpresaController do
  use WisdomToothWeb, :controller

  alias WisdomTooth.GestionJuicios
  alias WisdomTooth.GestionJuicios.Empresa

  def index(conn, params) do
    {empresas, pagination} = GestionJuicios.list_empresas_paginated(params)
    render(conn, "index.html", empresas: empresas, pagination: pagination, busqueda: nil)
  end

  def new(conn, _params) do
    changeset = GestionJuicios.change_empresa(%Empresa{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"empresa" => empresa_params}) do
    case GestionJuicios.create_empresa(empresa_params) do
      {:ok, empresa} ->
        conn
        |> put_flash(:info, "Empresa created successfully.")
        |> redirect(to: empresa_path(conn, :show, empresa))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    empresa = GestionJuicios.get_empresa!(id)
    juicios = GestionJuicios.list_juicios_for_rut(empresa.rut)
    render(conn, "show.html", empresa: empresa, juicios: juicios)
  end

  def edit(conn, %{"id" => id}) do
    empresa = GestionJuicios.get_empresa!(id)
    changeset = GestionJuicios.change_empresa(empresa)
    render(conn, "edit.html", empresa: empresa, changeset: changeset)
  end

  def update(conn, %{"id" => id, "empresa" => empresa_params}) do
    empresa = GestionJuicios.get_empresa!(id)

    case GestionJuicios.update_empresa(empresa, empresa_params) do
      {:ok, empresa} ->
        conn
        |> put_flash(:info, "Empresa updated successfully.")
        |> redirect(to: empresa_path(conn, :show, empresa))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", empresa: empresa, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    empresa = GestionJuicios.get_empresa!(id)
    {:ok, _empresa} = GestionJuicios.delete_empresa(empresa)

    conn
    |> put_flash(:info, "Empresa deleted successfully.")
    |> redirect(to: empresa_path(conn, :index))
  end

  def buscar(conn, %{"s" => busqueda, "page" => page}) do
    {empresas, pagination} = GestionJuicios.list_empresas_paginated_by_nombre(busqueda, %{"page" => page})

    render(conn, "index.html", empresas: empresas, pagination: pagination, busqueda: busqueda)
  end
  def buscar(conn, %{"s" => busqueda}), do: buscar(conn, %{"s" => busqueda, "page" => 1})
end
