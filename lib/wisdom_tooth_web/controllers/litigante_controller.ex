defmodule WisdomToothWeb.LitiganteController do
  use WisdomToothWeb, :controller

  alias WisdomTooth.GestionJuicios
  alias WisdomTooth.GestionJuicios.Litigante

  def index(conn, _params) do
    litigantes = GestionJuicios.list_litigantes()
    render(conn, "index.html", litigantes: litigantes)
  end

  def new(conn, _params) do
    changeset = GestionJuicios.change_litigante(%Litigante{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"litigante" => litigante_params}) do
    case GestionJuicios.create_litigante(litigante_params) do
      {:ok, litigante} ->
        conn
        |> put_flash(:info, "Litigante created successfully.")
        |> redirect(to: litigante_path(conn, :show, litigante))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    litigante = GestionJuicios.get_litigante!(id)
    render(conn, "show.html", litigante: litigante)
  end

  def edit(conn, %{"id" => id}) do
    litigante = GestionJuicios.get_litigante!(id)
    changeset = GestionJuicios.change_litigante(litigante)
    render(conn, "edit.html", litigante: litigante, changeset: changeset)
  end

  def update(conn, %{"id" => id, "litigante" => litigante_params}) do
    litigante = GestionJuicios.get_litigante!(id)

    case GestionJuicios.update_litigante(litigante, litigante_params) do
      {:ok, litigante} ->
        conn
        |> put_flash(:info, "Litigante updated successfully.")
        |> redirect(to: litigante_path(conn, :show, litigante))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", litigante: litigante, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    litigante = GestionJuicios.get_litigante!(id)
    {:ok, _litigante} = GestionJuicios.delete_litigante(litigante)

    conn
    |> put_flash(:info, "Litigante deleted successfully.")
    |> redirect(to: litigante_path(conn, :index))
  end
end
