defmodule WisdomToothWeb.EmailView do
  use WisdomToothWeb, :view

  def url_detalle_juicio(juicio) do
    case Application.get_env(:wisdom_tooth, :environment) do
      :dev ->
        uri = %URI{scheme: "http", host: "localhost:4000"}
        juicio_path(uri, :detalle, juicio)
      :prod -> "http://juicios.interlex.cl/juicios/detalle/#{juicio.id}"
    end

  end
end
