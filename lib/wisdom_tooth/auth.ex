defmodule WisdomTooth.Auth do
  import Plug.Conn
  import Comeonin.Pbkdf2, only: [check_pass: 2]
  import Phoenix.Controller  
  
  alias WisdomTooth.Cuentas
  alias WisdomToothWeb.Router.Helpers
  
  # Plug para cargar al usuario en la conexión
  def init(opts) do
    opts
  end
  
  def call(conn, _opts) do
    usuario_id = get_session(conn, :usuario_id)
    usuario = usuario_id && Cuentas.get_usuario!(usuario_id)
    assign(conn, :usuario, usuario)
  end
  
  # Funciones de inicio de sesión
  
  defp login(conn, usuario) do
    conn
    |> assign(:usuario, usuario)
    |> put_session(:usuario_id, usuario.id)
    |> configure_session(renew: true)
  end
  
  def logout(conn) do
    configure_session(conn, drop: true)
  end
  
  def login_by_email_and_password(conn, email, password) do
    estado = Cuentas.get_usuario_by_email(email) |> check_pass(password)
    
    case estado do
      {:ok, usuario} ->
        {:ok, login(conn, usuario)}
      {:error, motivo} ->
        {:error, motivo}
    end
  end
  
  # Function Plug que verifica si hay un usuario cargado
  
  def authenticate_user(conn, _opts) do
    if conn.assigns.usuario do
      conn
    else
      conn
      |> put_flash(:error, "Debes iniciar sesión para ver eso")
      |> IO.inspect()
      |> put_session(:redirect_to, conn.request_path)
      |> redirect(to: Helpers.sesion_path(conn, :new))
      |> halt()
    end
  end
  
end