defmodule WisdomTooth.Periodically do
  use GenServer

  import WisdomTooth.GestionJuicios

  def start_link do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    if Application.get_env(:wisdom_tooth, :environment) == :prod do
        Process.send_after(self(), :work, 10 * 1000)
    end
    {:ok, state}
  end

  def handle_info(:work, state) do
    cargar_empresas()
    cargar_juicios_todas_las_empresas()
    #cargar_metadata_juicios_en_curso()
    notificar_juicios_nuevos()


    schedule_work() # Reschedule once more
    {:noreply, state}
  end

  defp schedule_work() do
    IO.puts "Scheduled work in 6 hours"
    Process.send_after(self(), :work, 6 * 60 * 60 * 1000) # In 6 hours
  end
end
