defmodule WisdomTooth.Repo do
  use Ecto.Repo, otp_app: :wisdom_tooth
  use Phoenix.Pagination, per_page: 15

  @doc """
  Dynamically loads the repository url from the
  DATABASE_URL environment variable.
  """
  def init(_, opts) do
    {:ok, Keyword.put(opts, :url, System.get_env("DATABASE_URL"))}
  end
end
