defmodule WisdomTooth.Cuentas.Usuario do
  use Ecto.Schema
  import Ecto.Changeset


  schema "usuarios" do
    field :email, :string
    field :nombre, :string
    field :password, :string, virtual: true
    field :password_hash, :string

    timestamps()
  end

  @doc false
  def changeset(usuario, attrs) do
    usuario
    |> cast(attrs, [:nombre, :email])
    |> validate_required([:nombre, :email])
  end
  
  def registro_changeset(usuario, attrs) do
    usuario
    |> changeset(attrs)
    |> cast(attrs, [:password])
    |> validate_required(:password)
    |> validate_length(:password, min: 6, max: 100)
    |> put_pass_hash()
  end
  
  defp put_pass_hash(%Ecto.Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, Comeonin.Pbkdf2.add_hash(password))
  end
  defp put_pass_hash(changeset), do: changeset
end
