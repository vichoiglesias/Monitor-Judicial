defmodule WisdomTooth.Email do
  import Bamboo.Email
  use Bamboo.Phoenix, view: WisdomToothWeb.EmailView

  def email_juicios(email, juicios) do
    new_email
    |> to(email)
    |> from("Monitor Judicial · Interlex<no-reply@juicios.antifaz.cl>")
    |> subject("Nuevos Juicios Detectados")
    |> assign(:juicios, juicios)
    |> render(:nuevos_juicios)
  end

  def email_no_hay_juicios() do
    new_email
    |> to("vichoss@gmail.com")
    |> from("Monitor Judicial · Interlex<no-reply@juicios.antifaz.cl>")
    |> subject("No se han detectado nuevos juicios")
    |> render(:no_se_han_detectado_nuevos_juicios)
  end
end
