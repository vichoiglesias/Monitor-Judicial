defmodule WisdomTooth.RUT do
  def validar(rut) do
    {serie, dv} = rut |> dividir(:sin_validar)
    case obtener_dv(serie) do
      ^dv -> {:ok, rut}
        _ -> {:error, rut}
    end
  end

  def dividir(rut) do
    case validar(rut) do
      {:ok, rut_validado} ->
        dividir(rut_validado, :sin_validar)
      {:error, _} ->
        {"0", "0"}
    end
  end

  defp dividir(rut, :sin_validar) do
    rut_sf = sin_formato(rut)

    serie =
      String.slice(rut_sf, 0..-2)
      |> String.to_integer
      |> Integer.to_string

    dv = String.slice(rut_sf, -1..-1)
    {serie, dv}
  end

  def sin_formato(rut) do
    rut
    |> String.replace(~r/\s/, "")
    |> String.replace(~r/[\.|-]/, "")
    |> String.replace("k", "K")
  end

  def con_formato(rut) do
    {serie, dv} =
      rut
      |> sin_formato
      |> dividir

    serie_con_formato =
      serie
      |> String.to_charlist
      |> Enum.reverse
      |> Enum.chunk_every(3)
      |> Enum.join(".")
      |> String.reverse

    "#{serie_con_formato}-#{dv}"
  end

  @doc """
  Obtener el digito verificador de un RUT que no lo trae.
  """
  def obtener_dv(rut_sin_dv) do
    {listado, _} =
      rut_sin_dv
      |> String.codepoints
      |> Enum.reverse
      |> Enum.map_reduce(2, fn(digito, acc) ->
        {digito, _} = Integer.parse(digito)
        new_acc = if acc == 7, do: 2, else: acc + 1
        {digito * acc, new_acc}
      end)

    suma = listado
    |> Enum.reduce(0, &(&1 + &2))

    reminder = 11 - rem(suma, 11)

    case reminder do
      11 -> "0"
      10 -> "K"
      x  -> Integer.to_string(x)
    end
  end

end
