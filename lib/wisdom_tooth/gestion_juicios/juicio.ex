defmodule WisdomTooth.GestionJuicios.Juicio do
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias WisdomTooth.Repo
  alias WisdomTooth.RUT
  alias WisdomTooth.GestionJuicios.Litigante
  alias WisdomTooth.GestionJuicios.EscritoPorResolver

  schema "juicios" do
    field :caratulado, :string
    field :fecha, :date
    field :rol, :string
    field :rut, :string
    field :tribunal, :string
    field :notificado, :date
    field :last_seen, :date
    field :url, :string
    field :tipo, :string
    field :estado_administrativo, :string
    field :estado_procedimiento, :string
    field :etapa, :string
    field :url_demanda, :string
    field :procedimiento, :string
    field :ubicacion, :string
    has_many :litigantes, Litigante
    has_many :escritos_por_resolver, EscritoPorResolver

    field :tipo_procedimiento, :string

    field :contraparte, :string
    field :rut_contraparte, :string
    field :calidad, :string
    field :cuantia, :integer
    field :abogado_contraparte, :string

    timestamps()
  end

  @doc false
  def changeset(juicio, attrs) do
    changeset =
      juicio
      |> cast(attrs, [:rol, :fecha, :caratulado, :tribunal, :rut, :url, :tipo])
      |> validate_required([:rol, :fecha, :caratulado, :tribunal, :rut, :url, :tipo])
      |> unique_constraint(:rol, name: :juicios_rol_tribunal_rut_index)
      |> validate_rut(:rut)

    if changeset.valid? do
      format_rut(changeset, :rut)
    else
      changeset
    end
  end

  def validate_rut(changeset, field, options \\ []) do
    validate_change(changeset, field, fn _, rut ->
      case RUT.validar(rut) do
        {:ok, _} -> []
        {:error, _} -> [{field, options[:message] || "RUT inválido"}]
      end
    end)
  end

  def format_rut(changeset, field) do
    if rut = get_change(changeset, field) do
      put_change(changeset, field, RUT.con_formato(rut))
    else
      changeset
    end
  end

  def count_juicios_empresa(rut_empresa), do: Repo.aggregate(from(j in __MODULE__, where: j.rut == ^rut_empresa ), :count, :id)
  def count_juicios_civiles_empresa(rut_empresa), do: Repo.aggregate(from(j in __MODULE__, where: j.rut == ^rut_empresa and j.tipo == "Civil"), :count, :id)
  def count_juicios_laborales_empresa(rut_empresa), do: Repo.aggregate(from(j in __MODULE__, where: j.rut == ^rut_empresa and j.tipo == "Laboral"), :count, :id)

  def cargar_tipo_procedimiento(juicio) do
    proc = juicio.procedimiento

    tipo_procedimiento = cond do
      proc in ["Exhorto"] ->
        "Accesorio"
      proc in ["Reclamo"] ->
        "Administrativo"
      proc in ["Ordinario","Ordinario Menor Cuantía","Sumario","Ejecutivo Mínima Cuantía","Ordinario Menor Cuantia","Medida prejudicial","Ordinario Mayor Cuantía","Mínima Cuantía","Sumario de Arbitro","Gestión Voluntaria","Juicio de Arrendamiento"] ->
        "Civil"
      proc in ["Ejecutivo Obligación de Dar","Gestión Prep. not.protesto de ch,l y p.","Gestión Prep. recon de firma y conf de deuda","Gestión Preparatoria (Citac.Conf.Deuda)","Gestión Preparatoria Notificación Cobro de Factura","Gestion Preparatoria (Reconocimiento de Firma)","Ges. Prep.-Medida Prejud."] ->
        "Ejecutivo"
      proc in ["Monitorio", "Tutela", "Practica Antisindical"] ->
        "Laboral"
      proc in ["Concursal Reorganización", "Concursal de Liquidación Voluntaria - Persona Deudora", "Concursal De Liquidación Forzosa - Empresa Deudora"] ->
        "Quiebra"
    end

    Map.put(juicio, :tipo_procedimiento, tipo_procedimiento)
  end

  def load_interlex_corporativo_styled_data(juicio) do
    change(juicio)
    |> load_calidad()
    |> load_contraparte()
    |> load_abogado_contraparte()
    |> Repo.update

    juicio
  end

  def load_calidad(changeset) do
    juicio = changeset.data

    litigante = Repo.one(from Litigante, where: [juicio_id: ^juicio.id, rut: ^juicio.rut], limit: 1)
    if litigante do
      put_change(changeset, :calidad, litigante.rol)
    else
      changeset
    end
  end

  def load_contraparte(changeset) do
    juicio = changeset.data
    calidad = get_field(changeset, :calidad)

    calidad_contraparte = case calidad do
      "DTE." -> ["DDO."]
      "DDO." -> ["DTE."]
      "DDO.SO" -> ["DTE.", 	"DNCTE."]
      "DNCDO." -> ["DNCTE."]
      "ACRDOR" -> ["DDOR."]
      _ ->  [""]
    end

    litigante = Repo.one(from l in Litigante, where: l.juicio_id == ^juicio.id and l.rol in ^calidad_contraparte, limit: 1)
    if litigante do
      changeset
      |> put_change(:contraparte, litigante.nombre)
      |> put_change(:rut_contraparte, litigante.rut)
    else
      changeset
    end
  end

  def load_abogado_contraparte(changeset) do
    juicio = changeset.data
    calidad = get_field(changeset, :calidad)

    calidad_contraparte = case calidad do
      "DTE." -> ["AB.DDO", "AP.DDO"]
      "DDO." -> ["AB.DTE", "AP.DTE"]
      "DDO.SO" -> ["AB.DTE", "AP.DTE"]
      "DNCDO." -> ["AB.DNC"]
      "ACRDOR" -> ["AB.DDO", "AP.DDO"]
      _ ->  [""]
    end

    litigante = Repo.one(from l in Litigante, where: l.juicio_id == ^juicio.id and l.rol in ^calidad_contraparte, limit: 1)
    if litigante do
      changeset
      |> put_change(:abogado_contraparte, litigante.nombre)
    else
      changeset
    end
  end
end
