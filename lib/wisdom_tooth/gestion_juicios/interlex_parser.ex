defmodule WisdomTooth.GestionJuicios.InterlexParser do
  alias WisdomTooth.RUT
  alias WisdomTooth.Repo
  import Ecto.Query

  defp load_data, do: HTTPotion.get("http://www.interlex.cl/corporativo/scripts/listado_sociedades.asp")
  defp parse_data(%{body: body}) do
    body
    |> Floki.find("tr")
    |> Enum.map(fn(tr) ->
      nombre = Floki.find(tr, "td:first-child") |> Floki.text |> :unicode.characters_to_binary(:latin1)
      {status, rut} = Floki.find(tr, "td:nth-child(2)") |> Floki.text |> :unicode.characters_to_binary(:latin1) |> RUT.validar
      case status do
        :ok -> %{ nombre: nombre, rut: rut |> RUT.con_formato() }
        _ -> %{}
      end
    end)
  end

  def obtener_empresas do
    load_data() |> parse_data()
  end

  def notificar_juicios(query) do
    query = from j in query, where: j.procedimiento != "Exhorto"

    interlex_url = "http://www.interlex.cl/corporativo/scripts/nuevo_juicio_desde_monitor.asp"

    Repo.all(query)
    |> Enum.map(fn (juicio) ->
      URI.encode("#{interlex_url}?id_monitor=#{juicio.id}&rut=#{juicio.rut}&caratulado=#{juicio.caratulado}&contraparte=#{juicio.contraparte}&rut_contraparte=#{juicio.rut_contraparte}&materia=#{juicio.tipo}&tribunal=#{juicio.tribunal}&fecha=#{juicio.fecha}&rol=#{juicio.rol}&calidad=#{juicio.calidad}&abogado_contraparte=#{juicio.abogado_contraparte}&etapa=#{juicio.etapa}")
    end)
  end

end
