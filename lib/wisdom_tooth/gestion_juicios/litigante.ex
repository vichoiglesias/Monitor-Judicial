defmodule WisdomTooth.GestionJuicios.Litigante do
  use Ecto.Schema
  import Ecto.Changeset
  alias WisdomTooth.RUT
  alias WisdomTooth.GestionJuicios.Juicio

  schema "litigantes" do
    field :nombre, :string
    field :rol, :string
    field :rut, :string
    field :tipo_persona, :string
    belongs_to :juicio, Juicio

    timestamps()
  end

  @doc false
  def changeset(litigante, attrs) do
    changeset = litigante
    |> cast(attrs, [:rol, :rut, :tipo_persona, :nombre, :juicio_id])
    |> validate_required([:rol, :rut, :tipo_persona, :nombre, :juicio_id])
    |> unique_constraint(:rol, name: :litigantes_rol_rut_juicio_id_index)
    |> validate_rut(:rut)

    if changeset.valid? do
      format_rut(changeset, :rut)
    else
      changeset
    end
  end

  def validate_rut(changeset, field, options \\ []) do
    validate_change(changeset, field, fn _, rut ->
      case RUT.validar(rut) do
        {:ok, _} -> []
        {:error, _} -> [{field, options[:message] || "RUT inválido"}]
      end
    end)
  end

  def format_rut(changeset, field) do
    if rut = get_change(changeset, field) do
      put_change(changeset, field, RUT.con_formato(rut))
    else
      changeset
    end
  end
end
