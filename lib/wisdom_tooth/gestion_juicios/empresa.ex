defmodule WisdomTooth.GestionJuicios.Empresa do
  use Ecto.Schema
  import Ecto.Changeset


  schema "empresas" do
    field :nombre, :string
    field :rut, :string
    field :numero_juicios, :integer, virtual: true
    field :numero_juicios_laborales, :integer, virtual: true
    field :numero_juicios_civiles, :integer, virtual: true
    timestamps()
  end

  @doc false
  def changeset(empresa, attrs) do
    empresa
    |> cast(attrs, [:nombre, :rut])
    |> validate_required([:nombre, :rut])
    |> unique_constraint(:rut)
  end
end
