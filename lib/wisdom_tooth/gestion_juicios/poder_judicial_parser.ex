defmodule WisdomTooth.GestionJuicios.PoderJudicialParser do
  @moduledoc """
    Utility functions for downloading and parsing data from Chilean Courts (PDJ: Poder Judicial).

    Works with Civil and Labor Courts.
  """
  alias WisdomTooth.RUT
  alias WisdomTooth.GestionJuicios.Juicio

  @doc """
  Loads initial cookies required for further interaction with PDJ for Civil lawsuits
  """
  def initial_payload_civil do
    # step one: Load front page
    response = HTTPotion.get("http://civil.poderjudicial.cl/CIVILPORWEB/", timeout: 15_000)
    cookie = get_session_from_response(response)

    # step two: Load menu: this gets the session cookie we need!
    HTTPotion.get(
      "http://civil.poderjudicial.cl/CIVILPORWEB/AtPublicoViewAccion.do?tipoMenuATP=1",
      headers: [Cookie: cookie],
      timeout: 15_000
    )

    # ready to go!
    cookie
  end

  @doc """
  Parse the session cookie
  """
  defp get_session_from_response(%{:headers => %{:hdrs => %{"set-cookie" => cookie}}}) when is_list(cookie) do
    cookie
    |> Enum.find(fn c -> String.slice(c, 0, 10) == "JSESSIONID" end)
    |> String.split(";")
    |> List.first()
  end

  defp get_session_from_response(%{:headers => %{:hdrs => %{"set-cookie" => cookie}}}) do
    cookie
    |> String.split(";")
    |> List.first()
  end

  @doc """
  Returns the list of lawsuits from Civil and Labor courts by RUT (RUT = National ID Number)
  """
  def find_for_rut(rut \\ "90.193.000-7") do
    cookie = initial_payload_civil()

    {serie, dv} = RUT.dividir(rut)


    civiles = HTTPotion.post(
      "http://civil.poderjudicial.cl/CIVILPORWEB/AtPublicoDAction.do",
      body: "RUT_Consulta=#{serie}&RUT_DvConsulta=#{dv}&TIP_Consulta=3&irAccionAtPublico=Consulta",
      headers: [
        "User-Agent": "Wisdom T",
        "Content-Type": "application/x-www-form-urlencoded",
        Cookie: cookie
      ],
      timeout: 1_000_000
    )
    |> process_data(rut)

    laborales = find_for_rut_laboral(rut)

    civiles ++ laborales
  end

  @doc """
  Parses data from a list of Civil lawsuits for a RUT
  """
  def process_data(%{body: body}, rut) do
    {:ok, _} = RUT.validar(rut)

    body
    |> add_missing_closing_tr
    |> Floki.find("#contentCellsAddTabla tr")
    |> Enum.map(fn tr ->
      fecha =
        Floki.find(tr, "tr td:nth-child(2)")
        |> Floki.text
        |> String.split("/")
        |> Enum.map(fn(x)-> Integer.parse(x) |> elem(0)  end)

      %{
        rol: Floki.find(tr, "tr td:first-child a") |> Floki.text |> String.slice(0..-2) |> String.trim,
        fecha: Date.new(Enum.at(fecha, 2), Enum.at(fecha, 1), Enum.at(fecha, 0)) |> elem(1),
        caratulado: Floki.find(tr, "tr td:nth-child(3)") |> Floki.text |> :unicode.characters_to_binary(:latin1),
        tribunal: Floki.find(tr, "tr td:nth-child(4)") |> Floki.text |> :unicode.characters_to_binary(:latin1), #String.replace(<<186>>, <<194, 186>>),
        rut: RUT.con_formato(rut),
        url: Floki.find(tr, "tr td:first-child a") |> Floki.attribute("href") |> :unicode.characters_to_binary(:latin1),
        tipo: "Civil",
        last_seen: Date.utc_today()
      }
    end)
  end
  def process_data(%HTTPotion.ErrorResponse{message: message}, rut) do
    IO.puts "Error cargando datos para #{rut}: #{message}"
    []
  end

  @doc """
  PDJ's site doesn't close <tr>, so we have to close it before passing the data to Floki HTML parser.

  Conveniently, there is a script tag at the end of the row, so we replace it with the </tr> tag.
  """
  defp add_missing_closing_tr(body),
    do: String.replace(body, "<script>contArchivo++;</script>", "</tr>")

  @doc """
  Loads a lawsuit detail page
  """
  def cargar_pagina_detalle_juicio(url, id) do
    poder_judicial_url = case url do
      "/SITLAPORWEB/" <> _ -> "http://laboral.poderjudicial.cl/#{url}"
      "/CIVILPORWEB/" <> _ -> "http://civil.poderjudicial.cl/#{url}"
    end

    HTTPotion.get(
      poder_judicial_url,
      headers: [
        "User-Agent": "Wisdom T",
      ],
      timeout: 1_000_000
    )
    |> process_detalle_juicio(id)
  end

  @doc """
  Loads a lawsuit notebook
  """
  def cargar_cuaderno(params, id, url) do
    cookie = initial_payload_civil()

    HTTPotion.get(
      "http://civil.poderjudicial.cl/#{url}",
      headers: [
        "User-Agent": "Wisdom T",
        Cookie: cookie
      ],
      timeout: 1_000_000
    )

    HTTPotion.post(
      "http://civil.poderjudicial.cl/CIVILPORWEB/AtPublicoDAction.do",
      body: URI.encode_query(params),
      headers: [
        "User-Agent": "Wisdom T",
        Cookie: cookie,
        "Content-Type": "application/x-www-form-urlencoded",
      ],
      timeout: 1_000_000
    )
    |> process_detalle_juicio(id)
  end

  @doc """
  Parses a lawsuit detail page
  """
  def process_detalle_juicio(%{body: body}, id) do
    body
    |> :unicode.characters_to_binary(:latin1)
    |> String.replace("method=\"post\"", "method=\"get\"")
    |> String.replace("/CIVILPORWEB/AtPublicoDAction.do", "/juicios/detalle/#{id}/cuaderno")
    |> String.replace("/CIVILPORWEB/", "http://civil.poderjudicial.cl/CIVILPORWEB/")
    |> String.replace("/SITLAPORWEB/", "http://laboral.poderjudicial.cl/SITLAPORWEB/")
    |> String.replace("top.body.", "")
  end
  def process_detalle_juicio(%HTTPotion.ErrorResponse{message: message}, id) do
    IO.puts "Error cargando juicio id #{id}: #{message}"
    ""
  end

  @doc """
  Loads initial cookies required for further interaction with PDJ for labor courts
  """
  def initial_payload_laboral do
    # step one
    response = HTTPotion.get("http://laboral.poderjudicial.cl/SITLAPORWEB/InicioAplicacionPortal.do?FLG_Autoconsulta=1", timeout: 15_000)
    cookie = get_session_from_response(response)

    HTTPotion.get(
      "http://laboral.poderjudicial.cl/SITLAPORWEB/AtPublicoViewAccion.do?tipoMenuATP=1",
      headers: [Cookie: cookie],
      timeout: 15_000
    )
    cookie
  end

  @doc """
  Loads laboral lawsuits
  """
  def find_for_rut_laboral(rut \\ "99.568.720-8") do
    cookie = initial_payload_laboral()

    {serie, dv} = RUT.dividir(rut)

    HTTPotion.post(
      "http://laboral.poderjudicial.cl/SITLAPORWEB/AtPublicoDAction.do",
      body: "RUT_Consulta=#{serie}&RUT_DvConsulta=#{dv}&TIP_Consulta=2&irAccionAtPublico=Consulta&TIP_Lengueta=tdTres",
      headers: [
        "User-Agent": "Wisdom T",
        "Content-Type": "application/x-www-form-urlencoded",
        Cookie: cookie
      ],
      timeout: 1_000_000
    )
    |> process_data_laboral(rut)
  end

  @doc """
  Parses data from a list of Labor lawsuits for a RUT
  """
  def process_data_laboral(%{body: body}, rut) do
    {:ok, _} = RUT.validar(rut)

    body
    |> Floki.find(".filadostabla, .filaunotabla")
    |> Enum.map(fn tr ->
      fecha =
        Floki.find(tr, "tr td:nth-child(3)")
        |> Enum.at(0)
        |> Floki.text
        |> String.split("/")
        |> Enum.map(fn(x)-> Integer.parse(x) |> elem(0)  end)
      %{
        rol: Floki.find(tr, "tr td:first-child a") |> Enum.at(0) |> Floki.text |> String.trim,
        fecha: Date.new(Enum.at(fecha, 2), Enum.at(fecha, 1), Enum.at(fecha, 0)) |> elem(1),
        caratulado: Floki.find(tr, "tr  td:nth-child(4)") |> Enum.at(0) |> Floki.text |> :unicode.characters_to_binary(:latin1),
        tribunal: Floki.find(tr, "tr  td:nth-child(5)") |> Enum.at(0) |> Floki.text |> :unicode.characters_to_binary(:latin1), #String.replace(<<186>>, <<194, 186>>),
        rut: RUT.con_formato(rut),
        url: Floki.find(tr, "tr td:first-child a") |> Enum.at(0) |> Floki.attribute("href") |> :unicode.characters_to_binary(:latin1) |> String.trim,
        tipo: "Laboral",
        last_seen: Date.utc_today()
      }
    end)
  end
  def process_data_laboral(%HTTPotion.ErrorResponse{message: message}, rut) do
    IO.puts "Error cargando datos para #{rut}: #{message}"
    []
  end

  @doc """
  Gets metadata for a lawsuit
  """
  def get_metadata_of_juicio(%Juicio{} = juicio) do
    IO.puts "Getting Metadata of juicio.id = #{juicio.id}"
    detalle_juicio = cargar_pagina_detalle_juicio(juicio.url, juicio.id)
    IO.puts " --- Metadata OK of juicio.id = #{juicio.id}"

    {_, _, metadata} =
      {juicio, detalle_juicio, %{}}
      |> obtener_estado_juicio
      #|> obtener_escritos_por_resolver
      #|> obtener_historia

    metadata
  end

  @doc """
  Gets current status of a lawsuit
  """
  defp obtener_estado_juicio({juicio, detalle_juicio, metadata}) do
    est_adm = case juicio.tipo do
      "Laboral" -> "Est. Adm. :"
      "Civil" -> "Est.Adm.:"
    end

    estado_administrativo =
      detalle_juicio
      |> Floki.find("td:fl-contains('#{est_adm}')")
      |> Floki.text
      |> String.replace(est_adm, "")
      |> String.trim

    procedimiento =
      detalle_juicio
      |> Floki.find("td:fl-contains('Proc.:')")
      |> List.first
      |> Floki.text
      |> String.replace("Proc.:", "")
      |> String.trim

    ubicacion =
      detalle_juicio
      |> Floki.find("td:fl-contains('Ubicación:')")
      |> Floki.text
      |> String.replace("Ubicación:", "")
      |> String.trim

    estado_procedimiento =
      detalle_juicio
      |> Floki.find("td:fl-contains('Estado Proc.:')")
      |> Floki.text
      |> String.replace("Estado Proc.:", "")
      |> String.trim

    etapa =
      detalle_juicio
      |> Floki.find("td:fl-contains('Etapa:')")
      |> Floki.text
      |> String.replace("Etapa:", "")
      |> String.trim

    url_demanda =
      case juicio.tipo do
        "Laboral" -> ""
        "Civil" ->
          ld =
            detalle_juicio
            |> Floki.find("td:fl-contains('Texto Demanda') img")
            |> Floki.attribute("onclick")
            |> List.first
          if ld do
            Regex.run(~r/'(.*)'/, ld, capture: :all_but_first) |> List.first
          end
      end

    metadata =
      metadata
      |> Map.put(:estado_administrativo, estado_administrativo)
      |> Map.put(:procedimiento, procedimiento)
      |> Map.put(:ubicacion, ubicacion)
      |> Map.put(:estado_procedimiento, estado_procedimiento)
      |> Map.put(:etapa, etapa)
      |> Map.put(:url_demanda, url_demanda)

    {juicio, detalle_juicio, metadata}
  end

  @doc """
  Gets lastest news of a lawsuit
  """
  def obtener_escritos_por_resolver(%Juicio{} = juicio) do
    detalle_juicio = cargar_pagina_detalle_juicio(juicio.url, juicio.id)

    {_, _, metadata} =
      {juicio, detalle_juicio, %{}} |> obtener_escritos_por_resolver

    metadata.escritos_por_resolver
  end
  def obtener_escritos_por_resolver({juicio, detalle_juicio, metadata}) do
    escritos_por_resolver =
      detalle_juicio
      |> Floki.find("#Escritos table:nth-child(2) tr")
      |> Enum.map(fn tr ->
        fecha =
          Floki.find(tr, "td:nth-child(3)")
          |> Enum.at(0)
          |> Floki.text
          |> String.trim
          |> String.split("/")
          |> Enum.map(fn(x)-> Integer.parse(x) |> elem(0)  end)

        documento =
          Floki.find(tr, "td:nth-child(1) img")
          |> Floki.attribute("onclick")
          |> List.first
        documento = if documento, do: Regex.run(~r/'(.*)'/, documento, capture: :all_but_first) |> List.first

        anexo =
          Floki.find(tr, "td:nth-child(2) img")
          |> Floki.attribute("onclick")
          |> List.first
        anexo = if anexo, do: Regex.run(~r/'(.*)'/, anexo, capture: :all_but_first) |> List.first

        %{
          documento: documento,
          anexo: anexo,
          fecha_ingreso: Date.new(Enum.at(fecha, 2), Enum.at(fecha, 1), Enum.at(fecha, 0)) |> elem(1),
          tipo_escrito: Floki.find(tr, "td:nth-child(4)") |> Floki.text |> String.trim,
          solicitante: Floki.find(tr, "td:nth-child(5)") |> Floki.text |> String.trim,
          juicio_id: juicio.id
        }
      end)

    {juicio, detalle_juicio, Map.put(metadata, :escritos_por_resolver, escritos_por_resolver)}
  end

  @doc """
  Gets all history of a lawsuit
  """
  def obtener_historia(%Juicio{} = juicio) do
    detalle_juicio = cargar_pagina_detalle_juicio(juicio.url, juicio.id)

    {_, _, metadata} =
      {juicio, detalle_juicio, %{}} |> obtener_historia

    metadata.historia
  end
  def obtener_historia({juicio, detalle_juicio, metadata}) do
    historia =
      detalle_juicio
      |> Floki.find("#Historia table:nth-child(2) tr")
      |> Enum.map(fn tr ->
        fecha =
          Floki.find(tr, "td:nth-child(7)")
          |> Enum.at(0)
          |> Floki.text
          |> String.trim
          |> String.split("/")
          |> Enum.map(fn(x)-> Integer.parse(x) |> elem(0)  end)

        documento =
          Floki.find(tr, "td:nth-child(2) img")
          |> Floki.attribute("onclick")
          |> List.first
        documento = if documento, do: Regex.run(~r/'(.*)'/, documento, capture: :all_but_first) |> List.first

        anexo =
          Floki.find(tr, "td:nth-child(3) img")
          |> Floki.attribute("onclick")
          |> List.first
        anexo = if anexo, do: Regex.run(~r/'(.*)'/, anexo, capture: :all_but_first) |> List.first

        %{
          folio: Floki.find(tr, "td:nth-child(1)") |> Floki.text |> String.trim,
          documento: documento,
          anexo: anexo,
          etapa: Floki.find(tr, "td:nth-child(4)") |> Floki.text |> String.trim,
          tramite: Floki.find(tr, "td:nth-child(5)") |> Floki.text |> String.trim,
          descripcion_tramite: Floki.find(tr, "td:nth-child(6)") |> Floki.text |> String.trim,
          fecha_tramite: fecha,
          fecha: Floki.find(tr, "td:nth-child(7)") |> Floki.text |> String.trim,
          foja: Floki.find(tr, "td:nth-child(8)") |> Floki.text |> String.trim,
        }
      end)

    {juicio, detalle_juicio, Map.put(metadata, :historia, historia)}
  end

  @doc """
  Gets participants in a lawsuit
  """
  def get_litigantes_of_juicio(%Juicio{} = juicio) do
    detalle_juicio = cargar_pagina_detalle_juicio(juicio.url, juicio.id)

    {_, _, metadata} = get_litigantes_of_juicio({juicio, detalle_juicio, %{}})

    metadata.litigantes
  end
  def get_litigantes_of_juicio({juicio, detalle_juicio, metadata}) do
    litigantes =
      detalle_juicio
      |> Floki.find("#Litigantes tr")
      |> tl
      |> Enum.map(fn tr ->
        IO.inspect juicio
        case juicio.tipo do
          "Laboral" ->
            %{
              rol: Floki.find(tr, "td:nth-child(3)") |> Floki.text |> String.trim,
              rut: Floki.find(tr, "td:nth-child(4)") |> Floki.text |> String.trim |> RUT.con_formato,
              tipo_persona: Floki.find(tr, "td:nth-child(5)") |> Floki.text |> String.trim,
              nombre: Floki.find(tr, "td:nth-child(6)") |> Floki.text |> String.trim,
              juicio_id: juicio.id
            }
          "Civil" ->
            %{
              rol: Floki.find(tr, "td:first-child") |> Floki.text |> String.trim,
              rut: Floki.find(tr, "td:nth-child(2)") |> Floki.text |> String.trim  |> IO.inspect |> RUT.con_formato,
              tipo_persona: Floki.find(tr, "td:nth-child(3)") |> Floki.text |> String.trim,
              nombre: Floki.find(tr, "td:nth-child(4)") |> Floki.text |> String.trim,
              juicio_id: juicio.id
            }
        end
      end)
      |> Enum.uniq

    {juicio, detalle_juicio, Map.put(metadata, :litigantes, litigantes)}
  end
end
