defmodule WisdomTooth.GestionJuicios.Exhorto do
  use Ecto.Schema
  import Ecto.Changeset


  schema "exhortos" do
    field :estado, :string
    field :fecha_ingreso, :date
    field :fecha_orden, :date
    field :tribunal, :string
    field :juicio_origen_id, :id
    field :juicio_destino_id, :id

    timestamps()
  end

  @doc false
  def changeset(exhorto, attrs) do
    exhorto
    |> cast(attrs, [:fecha_orden, :fecha_ingreso, :tribunal, :estado])
    |> validate_required([:fecha_orden, :fecha_ingreso, :tribunal, :estado])
  end
end
