defmodule WisdomTooth.GestionJuicios do
  @moduledoc """
  The GestionJuicios context.
  """

  import Ecto.Query, warn: false
  alias WisdomTooth.Repo

  alias WisdomTooth.GestionJuicios.Juicio
  alias WisdomTooth.GestionJuicios.Litigante
  alias WisdomTooth.GestionJuicios.EscritoPorResolver

  alias WisdomTooth.GestionJuicios.PoderJudicialParser
  alias WisdomTooth.GestionJuicios.InterlexParser

  alias WisdomTooth.Mailer
  alias WisdomTooth.Email

  @doc """
  Returns the list of juicios.

  ## Examples

      iex> list_juicios()
      [%Juicio{}, ...]

  """
  def list_juicios do
    Repo.all(Juicio)
  end

  def list_juicios_paginated(params) do
    Juicio
    |> order_by(desc: :fecha)
    |> Repo.paginate(params)
  end

  def list_juicios_paginated_by_rol_o_caratulado(busqueda, params) do
    busqueda = "%" <> busqueda <> "%"
    from(j in Juicio,
      where: ilike(j.rol, ^busqueda),
      or_where: ilike(j.caratulado, ^busqueda)
    )
    |> order_by(desc: :fecha)
    |> Repo.paginate(params)
  end

  def list_juicios_for_rut(rut) do
    Repo.all(from Juicio,
      where: [rut: ^rut],
      order_by: [desc: :fecha]
    ) |> Enum.map(&Juicio.cargar_tipo_procedimiento/1)

  end

  @doc """
  Gets a single juicio.

  Raises `Ecto.NoResultsError` if the Juicio does not exist.

  ## Examples

      iex> get_juicio!(123)
      %Juicio{}

      iex> get_juicio!(456)
      ** (Ecto.NoResultsError)

  """
  def get_juicio!(id), do: Repo.get!(Juicio, id)

  @doc """
  Creates a juicio.

  ## Examples

      iex> create_juicio(%{field: value})
      {:ok, %Juicio{}}

      iex> create_juicio(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_juicio(attrs \\ %{}) do
    %Juicio{}
    |> Juicio.changeset(attrs)
    |> Repo.insert()
  end

  def cargar_juicios(listado_juicios) when is_list(listado_juicios) do
    listado_juicios
    |> Enum.map(&cargar_juicios/1)
  end
  def cargar_juicios(%{rut: rut}), do: cargar_juicios(rut)
  def cargar_juicios(rut) do
    PoderJudicialParser.find_for_rut(rut)
    |> Enum.each(fn (juicio) ->
      case create_juicio(juicio) do
        {:ok, juicio} ->
          # nuevo juicio detectado!
          metadata = PoderJudicialParser.get_metadata_of_juicio(juicio) |> Map.to_list
          from(Juicio, where: [id: ^juicio.id])
          |> Repo.update_all(set: metadata)


          juicio
          |> cargar_litigantes()
          |> Juicio.load_interlex_corporativo_styled_data()

        {:error, _} ->
          # si falla es porque ya está en la db
          query = from(j in Juicio,
            where: [
              rol: ^juicio.rol,
              rut: ^juicio.rut,
              tribunal: ^juicio.tribunal
            ]
          )

          query
          |> Repo.update_all(set: [last_seen: Date.utc_today()])
      end
    end)
  end

  def cargar_juicios_todas_las_empresas do
    empresas = list_empresas()

    numero_empresas  = length(empresas)
    numero_sublistas = 5
    chunk_size       = Integer.floor_div(numero_empresas, numero_sublistas)

    empresas
    |> Enum.chunk_every(chunk_size)
    |> Enum.map(&(Task.async(WisdomTooth.GestionJuicios, :cargar_juicios, [&1])))
    |> Enum.map(&(Task.await(&1, :infinity)))
    #|> Enum.map(&cargar_juicios/1)
  end

  def notificar_juicios_nuevos do
    procedimientos = ["Gestión Prep. not.protesto de ch,l y p.","Gestión Prep. recon de firma y conf de deuda","Gestión Preparatoria (Citac.Conf.Deuda)","Ejecutivo Mínima Cuantía","Gestión Preparatoria Notificación Cobro de Factura","Concursal Reorganización", "Ejecutivo Obligación de Dar"]

    query =
      from j in Juicio,
      where: is_nil(j.notificado) and not j.procedimiento in ^procedimientos

    InterlexParser.notificar_juicios(query)

    por_notificar = Repo.all(query |> order_by(desc: :fecha))

    unless Enum.empty?(por_notificar) do
      Email.email_juicios("Vicente Iglesias<vichoss@gmail.com>, Marco Antonio Iglesias<miglesias@interlex.cl>, Francisca Pino<fpino@interlex.cl>, Samuel Iglesias<siglesias@interlex.cl>, Carolina Durán<cduran@interlex.cl>", por_notificar)
      |> Mailer.deliver_now

      query |> Repo.update_all(set: [notificado: Date.utc_today()])
    else
      Email.email_no_hay_juicios()
      |> Mailer.deliver_now
    end
  end

  @doc """
  Updates a juicio.

  ## Examples

      iex> update_juicio(juicio, %{field: new_value})
      {:ok, %Juicio{}}

      iex> update_juicio(juicio, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_juicio(%Juicio{} = juicio, attrs) do
    juicio
    |> Juicio.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Juicio.

  ## Examples

      iex> delete_juicio(juicio)
      {:ok, %Juicio{}}

      iex> delete_juicio(juicio)
      {:error, %Ecto.Changeset{}}

  """
  def delete_juicio(%Juicio{} = juicio) do
    Repo.delete(juicio)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking juicio changes.

  ## Examples

      iex> change_juicio(juicio)
      %Ecto.Changeset{source: %Juicio{}}

  """
  def change_juicio(%Juicio{} = juicio) do
    Juicio.changeset(juicio, %{})
  end

  alias WisdomTooth.GestionJuicios.Empresa

  @doc """
  Returns the list of empresas.

  ## Examples

      iex> list_empresas()
      [%Empresa{}, ...]

  """
  def list_empresas do
    Repo.all(Empresa)
  end

  def list_empresas_paginated(params) do
    {empresas, paginate} =
      Empresa
      |> order_by(asc: :nombre)
      |> Repo.paginate(params)

    empresas =
      empresas
      |> Enum.map(fn (empresa) ->
        empresa
        |> Map.put(:numero_juicios_laborales, Juicio.count_juicios_laborales_empresa(empresa.rut))
        |> Map.put(:numero_juicios_civiles, Juicio.count_juicios_civiles_empresa(empresa.rut))
        |> Map.put(:numero_juicios, Juicio.count_juicios_empresa(empresa.rut))
      end)

    {empresas, paginate}
  end

  def list_empresas_paginated_by_nombre(busqueda, params) do
    busqueda = "%" <> busqueda <> "%"

    {empresas, paginate} =
      from(e in Empresa, where: ilike(e.nombre, ^busqueda))
      |> order_by(asc: :nombre)
      |> Repo.paginate(params)

    empresas =
      empresas
      |> Enum.map(fn (empresa) ->
        empresa
        |> Map.put(:numero_juicios_laborales, Juicio.count_juicios_laborales_empresa(empresa.rut))
        |> Map.put(:numero_juicios_civiles, Juicio.count_juicios_civiles_empresa(empresa.rut))
        |> Map.put(:numero_juicios, Juicio.count_juicios_empresa(empresa.rut))
      end
      )
    {empresas, paginate}
  end

  @doc """
  Gets a single empresa.

  Raises `Ecto.NoResultsError` if the Empresa does not exist.

  ## Examples

      iex> get_empresa!(123)
      %Empresa{}

      iex> get_empresa!(456)
      ** (Ecto.NoResultsError)

  """
  def get_empresa!(id), do: Repo.get!(Empresa, id)

  @doc """
  Creates a empresa.

  ## Examples

      iex> create_empresa(%{field: value})
      {:ok, %Empresa{}}

      iex> create_empresa(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_empresa(attrs \\ %{}) do
    %Empresa{}
    |> Empresa.changeset(attrs)
    |> Repo.insert()
  end

  def cargar_empresas do
    InterlexParser.obtener_empresas
    |> Enum.each(fn (empresa) ->
      {code, changeset} = create_empresa(empresa)
      unless code == :ok do
        IO.inspect changeset
      end
    end)
  end

  @doc """
  Updates a empresa.

  ## Examples

      iex> update_empresa(empresa, %{field: new_value})
      {:ok, %Empresa{}}

      iex> update_empresa(empresa, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_empresa(%Empresa{} = empresa, attrs) do
    empresa
    |> Empresa.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Empresa.

  ## Examples

      iex> delete_empresa(empresa)
      {:ok, %Empresa{}}

      iex> delete_empresa(empresa)
      {:error, %Ecto.Changeset{}}

  """
  def delete_empresa(%Empresa{} = empresa) do
    Repo.delete(empresa)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking empresa changes.

  ## Examples

      iex> change_empresa(empresa)
      %Ecto.Changeset{source: %Empresa{}}

  """
  def change_empresa(%Empresa{} = empresa) do
    Empresa.changeset(empresa, %{})
  end


  @doc """
  Returns the list of litigantes.

  ## Examples

      iex> list_litigantes()
      [%Litigante{}, ...]

  """
  def list_litigantes do
    Repo.all(Litigante)
  end

  @doc """
  Gets a single litigante.

  Raises `Ecto.NoResultsError` if the Litigante does not exist.

  ## Examples

      iex> get_litigante!(123)
      %Litigante{}

      iex> get_litigante!(456)
      ** (Ecto.NoResultsError)

  """
  def get_litigante!(id), do: Repo.get!(Litigante, id)

  @doc """
  Creates a litigante.

  ## Examples

      iex> create_litigante(%{field: value})
      {:ok, %Litigante{}}

      iex> create_litigante(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_litigante(attrs \\ %{}) do
    %Litigante{}
    |> Litigante.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a litigante.

  ## Examples

      iex> update_litigante(litigante, %{field: new_value})
      {:ok, %Litigante{}}

      iex> update_litigante(litigante, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_litigante(%Litigante{} = litigante, attrs) do
    litigante
    |> Litigante.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Litigante.

  ## Examples

      iex> delete_litigante(litigante)
      {:ok, %Litigante{}}

      iex> delete_litigante(litigante)
      {:error, %Ecto.Changeset{}}

  """
  def delete_litigante(%Litigante{} = litigante) do
    Repo.delete(litigante)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking litigante changes.

  ## Examples

      iex> change_litigante(litigante)
      %Ecto.Changeset{source: %Litigante{}}

  """
  def change_litigante(%Litigante{} = litigante) do
    Litigante.changeset(litigante, %{})
  end

  def cargar_litigantes(juicios) when is_list(juicios) do
    Enum.map(juicios, &cargar_litigantes/1)
  end

  def cargar_litigantes(%Juicio{} = juicio) do
    PoderJudicialParser.get_litigantes_of_juicio(juicio)
    |> Enum.map(fn litigante ->
      {code, changeset} = create_litigante(litigante)
      unless code == :ok do
        IO.inspect changeset
      end
    end)

    juicio
  end

  def cargar_litigantes_todos_los_juicios do
    list_juicios()
    |> Enum.map(&cargar_litigantes/1)
  end

  def cargar_litigantes_juicios_sin_litigantes do
    query = from j in Juicio,
      left_join: l in Litigante,
      on: [juicio_id: j.id],
      where: is_nil(l.id)

    juicios = Repo.all query

    numero_juicios   = length(juicios)
    numero_sublistas = 5
    chunk_size       = Integer.floor_div(numero_juicios, numero_sublistas)

    juicios
    #|> Enum.chunk_every(chunk_size)
    #|> Enum.map(&(Task.async(WisdomTooth.GestionJuicios, :cargar_litigantes, [&1])))
    #|> Enum.map(&(Task.await(&1, :infinity)))
    |> Enum.map(&cargar_litigantes/1)
  end

  def cargar_litigantes_juicios_sin_litigantes(%Empresa{} = empresa) do
    query = from j in Juicio,
      left_join: l in Litigante,
      on: [juicio_id: j.id],
      where: is_nil(l.id) and j.rut == ^(empresa.rut)

    query
    |> Repo.all
    |> Enum.map(&cargar_litigantes/1)
  end

  def create_escrito_por_resolver(attrs \\ %{}) do
    %EscritoPorResolver{}
    |> EscritoPorResolver.changeset(attrs)
    |> Repo.insert()
  end

  def cargar_escritos_por_resolver(%Juicio{} = juicio) do
    PoderJudicialParser.obtener_escritos_por_resolver(juicio)
    |> Enum.map(&create_escrito_por_resolver/1)
  end

  def cargar_escritos_por_resolver_todos_los_juicios do
    query = from Juicio,
      order_by: [desc: :fecha],
      limit: 1000,
      offset: 1000

    query
    |> Repo.all
    |> Enum.map(&cargar_escritos_por_resolver/1)
  end

  def juicios_en_curso do
    from j in Juicio, where: not j.estado_procedimiento in ^["Arch. Judic.", "Concluido", "Nulo o sin efecto"] and not ilike(j.etapa, "Terminada")
  end

  def juicios_en_curso_sin_exhortos do
    from j in juicios_en_curso(),
      where: j.procedimiento != ^"Exhorto"
  end

  def cargar_metadata_juicios_en_curso do
    # juicios = juicios_en_curso() |> Repo.all
    juicios = Repo.all(from j in Juicio, where: j.caratulado != "/ISAPRE MASVIDA")

    numero_juicios   = length(juicios)
    numero_sublistas = 5
    chunk_size       = Integer.floor_div(numero_juicios, numero_sublistas)

    juicios
    # |> Enum.map(&cargar_metadata_juicio/1)
    |> Enum.chunk_every(chunk_size)
    |> Enum.map(&(Task.async(WisdomTooth.GestionJuicios, :cargar_metadata_juicios, [&1])))
    |> Enum.map(&(Task.await(&1, :infinity)))

    IO.puts "Todo cargado"
  end

  def cargar_metadata_juicios(listado_juicios) when is_list(listado_juicios) do
    listado_juicios |> Enum.map(&cargar_metadata_juicio/1)
  end

  def cargar_metadata_juicio(juicio) do
    metadata = PoderJudicialParser.get_metadata_of_juicio(juicio) |> Map.to_list

    from(Juicio, where: [id: ^juicio.id])
    |> Repo.update_all(set: metadata)
  end

  def juicios_por_rut_litigante(rut) do
    litigante = Repo.all(from Litigante, where: [rut: ^rut])

    litigante
    |> Enum.map(fn (lit) ->
      juicio = Repo.get(Juicio, lit.juicio_id)
      if juicio.rut == "99.568.720-8" do
      # "#{juicio.rol}: #{juicio.estado_procedimiento}"
      # "#{juicio.rol}: #{(juicio.etapa |> Integer.parse |> elem(1) |> String.trim)}"
        historia = PoderJudicialParser.obtener_historia(juicio)
        if length(historia) > 0 do
          ultima_historia = hd(historia)
          "#{juicio.rol} · #{juicio.estado_procedimiento} · #{(juicio.etapa |> Integer.parse |> elem(1) |> String.trim)} · #{ultima_historia.fecha}: #{ultima_historia.descripcion_tramite}"
        else
          "#{juicio.rol} · #{juicio.estado_procedimiento} · #{(juicio.etapa |> Integer.parse |> elem(1) |> String.trim)} · Sin trámites"
        end

      end
    end)
    |> Enum.filter(& !is_nil(&1))
  end
end
