defmodule WisdomTooth.GestionJuicios.EscritoPorResolver do
  use Ecto.Schema
  import Ecto.Changeset
  alias WisdomTooth.GestionJuicios.Juicio

  schema "escritos_por_resolver" do
    field :anexo, :string
    field :documento, :string
    field :fecha_ingreso, :date
    field :notificado, :date
    field :solicitante, :string
    field :tipo_escrito, :string
    belongs_to :juicio, Juicio

    timestamps()
  end

  @doc false
  def changeset(escrito_por_resolver, attrs) do
    escrito_por_resolver
    |> cast(attrs, [:documento, :anexo, :fecha_ingreso, :tipo_escrito, :solicitante, :notificado, :juicio_id])
    |> validate_required([:documento, :anexo, :fecha_ingreso, :tipo_escrito, :solicitante, :juicio_id])
  end
end
