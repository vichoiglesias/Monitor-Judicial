import { Controller } from "stimulus"

export default class extends Controller {
  connect() {
    this.datatable = $("#listado_juicios").DataTable({
      lengthChange: false,
      language: {
        url: "/i18n/spanish.json"
      }
    });

    $('.ui.dropdown').dropdown();
  }

  change_filtro(event) {
    $(".change_filtro .active").removeClass("active");
    $(event.target).addClass("active");

    this.datatable
      .columns(0)
      .search(event.target.dataset.filtro)
      .draw();
  }

  change_filtro_procedimiento(event) {
    this.datatable
      .columns(1)
      .search(event.target.dataset.filtro)
      .draw();
  }
}
